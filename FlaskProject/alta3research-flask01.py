#!/usr/bin/python3
from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__)
books =[]
# return home.html (landing page)
@app.route('/')
def home():
    return render_template('home.html')

# return books.html (To add book record)
@app.route('/enternew')
def newBook():
    return render_template('booksPost.html')

# if someone uses books.html it will generate a POST
# this post will be sent to /booksPost
# where the information will be displayed to user
@app.route('/booksPost', methods = ['POST'])
def addrec():
    try:
        nm = request.form['name']         # student name
        price = request.form['price']     # student street address
        auth = request.form['author']     # student city
        yr = request.form['year']       # "pin" assigned to student
        msg = "Record added successfully"
        books.append(nm)
        print(f"Book Name: {nm}\nPrice: {price}\nAuthor: {auth}\nyear: {yr}")
    except:
        msg = "Unable to add record"
    finally:
        return render_template("result.html", msg=msg)

@app.route('/getBooks')
def getBooks():
    return render_template("result.html", msg=f"List of book names which you've added \n {books}")

if __name__ == '__main__':
    try:
        app.run(host="0.0.0.0", port=2121, debug = True)
    except:
        print("App failed on boot")
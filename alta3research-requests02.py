#!/usr/bin/python3
'''This project is displaying list of makeup brands and their product type and add it to cart'''

from prettytable import PrettyTable
import requests

url = "https://makeup-api.herokuapp.com/api/v1/products.json/"
cart = []

def main(finalUrl):
    #calling api to get data
    response = requests.get(finalUrl)
    jsonResp = response.json()

    #displaying data in pretty table
    t = PrettyTable(['Category', 'Price ($)', 'Type', 'Product Link'])
    for i in range(len(jsonResp)):
        #check non null values
        if not (jsonResp[i].get('category') and jsonResp[i].get('price') and jsonResp[i].get('price_sign') and jsonResp[i].get('product_link')) is None:
            t.add_row([jsonResp[i].get('category'), jsonResp[i].get('price'), jsonResp[i].get('product_type'), jsonResp[i].get('product_link')])
    print(t)
if __name__ =="__main__":
    print("-----Welcome to Makemeup store-----")
    print("Select ur option")
    print("___________________________________")
    while True:
        print("1. Get products by brand (eg : colourpop, boosh, deciem)"
              "\n2. Get product by type (eg: lip_liner, foundation)"
              "\n3. Items you added to cart"
              "\n4. Available Brands"
              "\n99. Exit")
        print("--------------------------------------------------------")

        userIn = input(">")
        if userIn == "1":
            brand = input("Enter brand name: ")
            print("Brand: ", brand)
            main(f"{url}?brand={brand}")
            addProduct = input("Do you want to add this brand to cart. 1-Yes 0-No : ")
            if addProduct == "1":
                cart.append(brand)

        if userIn == "2":
            type = input("Enter product type: ")
            main(f"{url}?product_type={type}")
            addProduct = input("Do you want to add this type to cart. 1-Yes 0-No : ")
            if addProduct == "1":
                cart.append(type)
        if userIn == "3":
            if not cart:
                print("You haven't added any brand to cart")
            else:
                print(cart)
        if userIn == "4":
            print("1. Colourpop\n2. Boosh \n3. Deciem")
        if userIn == "99":
            print("Exit")
            break

'''
OUTPUT: 

-----Welcome to Makemeup store-----
Select ur option
___________________________________
1. Get products by brand (eg : colourpop, boosh, deciem)
2. Get product by type (eg: lip_liner, foundation)
3. Items you added to cart
4. Available Brands
99. Exit
--------------------------------------------------------
>1
Enter brand name: colourpop
Brand:  colourpop
+----------+-----------+------------+------------------------------------------------------------------+
| Category | Price ($) |    Type    |                           Product Link                           |
+----------+-----------+------------+------------------------------------------------------------------+
|  pencil  |    5.0    | lip_liner  |         https://colourpop.com/collections/lippie-pencil          |
| lipstick |    5.5    |  lipstick  | https://colourpop.com/collections/lippie-stix?filter=blotted-lip |
| lipstick |    5.5    |  lipstick  |          https://colourpop.com/collections/lippie-stix           |
|  liquid  |    12.0   | foundation |    https://colourpop.com/products/no-filter-matte-foundation     |
+----------+-----------+------------+------------------------------------------------------------------+
Do you want to add this brand to cart. 1-Yes 0-No : 1
1. Get products by brand (eg : colourpop, boosh, deciem)
2. Get product by type (eg: lip_liner, foundation)
3. Items you added to cart
4. Available Brands
99. Exit
--------------------------------------------------------
>2
Enter product type: lip_liner
+----------+-----------+-----------+-------------------------------------------------------+
| Category | Price ($) |    Type   |                      Product Link                     |
+----------+-----------+-----------+-------------------------------------------------------+
|  pencil  |    5.0    | lip_liner |    https://colourpop.com/collections/lippie-pencil    |
|  pencil  |    0.0    | lip_liner | https://www.purpicks.com/product/luminary-lip-crayon/ |
+----------+-----------+-----------+-------------------------------------------------------+
Do you want to add this type to cart. 1-Yes 0-No : 1
1. Get products by brand (eg : colourpop, boosh, deciem)
2. Get product by type (eg: lip_liner, foundation)
3. Items you added to cart
4. Available Brands
99. Exit
--------------------------------------------------------
>3
['colourpop', 'lip_liner']
1. Get products by brand (eg : colourpop, boosh, deciem)
2. Get product by type (eg: lip_liner, foundation)
3. Items you added to cart
4. Available Brands
99. Exit
--------------------------------------------------------
>4
1. Colourpop
2. Boosh 
3. Deciem
1. Get products by brand (eg : colourpop, boosh, deciem)
2. Get product by type (eg: lip_liner, foundation)
3. Items you added to cart
4. Available Brands
99. Exit
--------------------------------------------------------
>99
Exit

Process finished with exit code 0
'''